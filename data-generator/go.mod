module gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator

go 1.16

require (
	github.com/algolia/algoliasearch-client-go/v3 v3.18.0
	github.com/mb-14/gomarkov v0.0.0-20210216094942-a5b484cc0243
	github.com/onsi/ginkgo v1.16.1 // indirect
	github.com/onsi/gomega v1.11.0 // indirect
	github.com/tjarratt/babble v0.0.0-20191209142150-eecdf8c2339d
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.4
)
