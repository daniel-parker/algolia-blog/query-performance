package generators

import (
	"context"
	"github.com/tjarratt/babble"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/models"
	"gorm.io/gorm"
)

func GenerateTags(ctx context.Context, db *gorm.DB) error {
	babbler := babble.NewBabbler()

	var tags []models.Tag

	for i := 0; i < 500; i++ {
		name := babbler.Babble()

		tag := models.Tag{
			Name: name,
		}

		tags = append(tags, tag)
	}

	result := db.WithContext(ctx).Create(&tags)

	if result.Error != nil {
		return result.Error
	}

	return nil
}
