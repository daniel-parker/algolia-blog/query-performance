package generators

import (
	"context"
	"github.com/tjarratt/babble"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/models"
	"gorm.io/gorm"
)

func GenerateUsers(ctx context.Context, db *gorm.DB) error {
	babbler := babble.NewBabbler()
	babbler.Separator = ""

	var users []models.User

	for i := 0; i < 50; i++ {
		firstName := babbler.Babble()
		lastName := babbler.Babble()
		userName := babbler.Babble()

		user := models.User{
			FirstName: firstName,
			LastName:  lastName,
			UserName:  userName,
		}

		users = append(users, user)
	}

	result := db.WithContext(ctx).Create(&users)

	if result.Error != nil {
		return result.Error
	}

	return nil
}
