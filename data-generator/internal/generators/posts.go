package generators

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/algolia/algoliasearch-client-go/v3/algolia/opt"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/algolia"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/models"
	"gorm.io/gorm"
)

func flushPostsToAlgolia(posts *[]models.DenormalizedPost) error {
	postsIndex := algolia.NewPostIndex()

	res, err := postsIndex.SaveObjects(
		*posts,
		opt.AutoGenerateObjectIDIfNotExist(false),
	)

	if err != nil {
		return err
	}

	err = res.Wait()

	return err
}

func flushPostsToDB(ctx context.Context, posts *[]models.Post, db *gorm.DB) error {
	var result *gorm.DB
	for retry := 0; retry < 3; retry++ {
		result = db.WithContext(ctx).CreateInBatches(*posts, 1000)

		if result.Error == nil {
			return nil
		}

		backoff := time.Duration(2000 * 2^retry)
		time.Sleep(backoff * time.Millisecond)
	}

	return result.Error
}

// Generate the forum posts and push them to both the DB and Algolia
func GeneratePosts(ctx context.Context, db *gorm.DB) error {
	chain, err := setup()
	if err != nil {
		return err
	}

	var allForums []models.Forum
	var allTags []models.Tag
	var allUsers []models.User

	db.WithContext(ctx).Find(&allForums)
	db.WithContext(ctx).Find(&allTags)
	db.WithContext(ctx).Find(&allUsers)

	log.Println("Generating posts")

	// Change this to generate a different amount of posts
	postsToCreate := 1000000

	forumsMap := map[uint]models.Forum{}
	for _, forum := range allForums {
		forumsMap[forum.ID] = forum
	}

	var postsToWriteAlgolia []models.DenormalizedPost
	var postsToWriteDB []models.Post

	for i := 0; i < postsToCreate; i++ {
		percent := (float32(i + 1) / float32(postsToCreate)) * 100.0
		fmt.Printf("\r %d/%d %f%%", i, postsToCreate, percent)

		// We generate random indexes to pick tag user and forum for this post
		tag := rand.Intn(len(allTags))
		user := rand.Intn(len(allUsers))
		forum := rand.Intn(len(allForums))

		post := &models.Post{
			Title:   GenerateHNStory(chain),
			Body:    GenerateHNStory(chain),
			UserID:  allUsers[user].ID,
			Tags:    []models.Tag{allTags[tag]},
			ForumID: &allForums[forum].ID,
		}

		postsToWriteDB = append(postsToWriteDB, *post)

		// Only write posts to db and algolia in batches to speed things up
		if i%1000 == 0 {
			err = flushPostsToDB(ctx, &postsToWriteDB, db)
			if err != nil {
				return err
			}

			for _, post := range postsToWriteDB {
				denormalizedPost := post.Denormalize(forumsMap)
				postsToWriteAlgolia = append(postsToWriteAlgolia, *denormalizedPost)
			}

			err = flushPostsToAlgolia(&postsToWriteAlgolia)
			if err != nil {
				return err
			}

			postsToWriteAlgolia = []models.DenormalizedPost{}
			postsToWriteDB = []models.Post{}
		}
	}

	// Flush any remaining posts to the db and algolia
	if len(postsToWriteDB) > 0 {
		err = flushPostsToDB(ctx, &postsToWriteDB, db)
		if err != nil {
			return err
		}

		for _, post := range postsToWriteDB {
			denormalizedPost := post.Denormalize(forumsMap)
			postsToWriteAlgolia = append(postsToWriteAlgolia, *denormalizedPost)
		}
	}

	if len(postsToWriteAlgolia) > 0 {
		err := flushPostsToAlgolia(&postsToWriteAlgolia)
		if err != nil {
			return err
		}
	}

	return nil
}
