package generators

import (
	"context"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/models"
	"gorm.io/gorm"
)

// Generate a fixed set of forums
func GenerateForums(ctx context.Context, db *gorm.DB) error {
	var topLevelForum []models.Forum

	announcements := models.Forum{
		Name: "Announcements",
		Children: []models.Forum{
			{
				Name: "Important",
			},
		},
	}

	topLevelForum = append(topLevelForum, announcements)

	questions := models.Forum{
		Name: "Questions",
		Children: []models.Forum{
			{
				Name: "Programming",
				Children: []models.Forum{
					{
						Name: ".NET",
					},
					{
						Name: "Javasript",
						Children: []models.Forum{
							{
								Name: "Node.js",
							},
						},
					},
					{
						Name: "Python",
					},
				},
			},
			{
				Name: "Databases",
				Children: []models.Forum{
					{
						Name: "PostgreSQL",
					},
					{
						Name: "MySQL",
					},
				},
			},
		},
	}

	topLevelForum = append(topLevelForum, questions)

	result := db.WithContext(ctx).Create(&topLevelForum)

	if result.Error != nil {
		return result.Error
	}

	return nil
}
