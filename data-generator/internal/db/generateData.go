package db

import (
	"context"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/internal/generators"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
)

// Do initial setup of the database including schema
func setup(ctx context.Context, db *gorm.DB) error {
	// Schema
	err := db.AutoMigrate(
		&models.User{},
		&models.Forum{},
		&models.Post{},
		&models.Tag{},
	)

	if err != nil {
		return err
	}

	// Tags
	var tags []models.Tag
	result := db.WithContext(ctx).Find(&tags)

	if result.Error != nil {
		return result.Error
	}

	if len(tags) == 0 {
		err := generators.GenerateTags(ctx, db)
		if err != nil {
			return err
		}
	}

	// Users
	var users []models.User
	result = db.WithContext(ctx).Find(&users)

	if result.Error != nil {
		return result.Error
	}

	if len(users) == 0 {
		err := generators.GenerateUsers(ctx, db)
		if err != nil {
			return err
		}
	}

	// Forum
	var forum []models.Forum
	result = db.WithContext(ctx).Find(&forum)

	if result.Error != nil {
		return result.Error
	}

	if len(forum) == 0 {
		err := generators.GenerateForums(ctx, db)
		if err != nil {
			return err
		}
	}

	return nil
}

// Main function for data generator
func GenerateData() {
	dsn := os.Getenv("DB_CONN_STRING")
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		PrepareStmt: true,
		Logger: logger.Default.LogMode(logger.Silent),
	})

	if err != nil {
		log.Panicln(err)
	}

	ctx := context.Background()

	err = setup(ctx, db)

	if err != nil {
		panic(err)
	}

	err = generators.GeneratePosts(ctx, db)

	if err != nil {
		panic(err)
	}
}
