package main

import "gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/internal/db"

func main() {
	db.GenerateData()
}
