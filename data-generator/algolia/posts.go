package algolia

import (
	"os"

	"github.com/algolia/algoliasearch-client-go/v3/algolia/search"
)

func NewClient() *search.Client {
	appId := os.Getenv("ALGOLIA_APP_ID")
	apiKey := os.Getenv("ALGOLIA_API_KEY")

	return search.NewClient(appId, apiKey)
}

func NewPostIndex() *search.Index {
	client := NewClient()
	postIndex := client.InitIndex("POSTS")

	return postIndex
}
