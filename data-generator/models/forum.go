package models

import "gorm.io/gorm"

type Forum struct {
	gorm.Model
	Name string
	ParentID *uint
	Children []Forum `gorm:"foreignKey:ParentID"`
	Posts []Post
}
