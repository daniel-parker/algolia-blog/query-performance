package models

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"strconv"
)

type Post struct {
	gorm.Model
	Title string
	Body string
	BodyTokens BodyTokens `gorm:"->:false;<-:create;type:TSVECTOR;index:,type:gin;"`
	UserID uint
	Tags []Tag `gorm:"many2many:post_tags;"`
	ForumID *uint
}

type DenormalizedPost struct {
	ObjectID string `json:"objectID"`
	CreatedAt int64 `json:"createdAt"`
	Title string	`json:"title"`
	Body string		`json:"body"`
	UserID uint		`json:"userId"`
	Tags []string 	`json:"_tags"`
	ForumID uint	`json:"forumId"`
}

type BodyTokens struct {
	Value string
}

func (bt BodyTokens) GormValue(ctx context.Context, db *gorm.DB) clause.Expr {
	return clause.Expr{
		SQL: "to_tsvector(?)",
		Vars: []interface{}{bt.Value},
	}
}

func (bt *BodyTokens) Scan(value interface{}) error {
	bytes, ok := value.([]byte)

	if !ok {
		return errors.New(fmt.Sprint("failed to unmarshal JSONB value: ", value))
	}

	var val string
	err := json.Unmarshal(bytes, &val)

	*bt = BodyTokens{
		Value: val,
	}

	return err
}

func (p *Post) BeforeCreate(tx *gorm.DB) error {
	p.BodyTokens = BodyTokens{
		Value: p.Body,
	}
	return nil
}

func (p *Post) BeforeUpdate(tx *gorm.DB) error {
	p.BodyTokens = BodyTokens{
		Value: p.Body,
	}
	return nil
}

func getAllParentForums(allForum map[uint]Forum, parentForum uint) []string {
	forums := []string{
		fmt.Sprintf("%d", parentForum),
	}

	currentForum := allForum[parentForum]

	for currentForum.ParentID != nil {
		forums = append(forums, fmt.Sprintf("%d", *currentForum.ParentID))
		currentForum = allForum[*currentForum.ParentID]
	}

	return forums
}

func (p *Post) Denormalize(forumsMap map[uint]Forum) *DenormalizedPost {
	parentForums := getAllParentForums(forumsMap, *p.ForumID)

	var tags []string
	for _, tag := range p.Tags {
		tags = append(tags, tag.Name)
	}

	for _, parentForum := range parentForums {
		tags = append(tags, parentForum)
	}

	return &DenormalizedPost{
		ObjectID:   strconv.FormatUint(uint64(p.ID), 10),
		CreatedAt:  p.CreatedAt.Unix(),
		Title:      p.Title,
		Body:       p.Body,
		UserID:     p.UserID,
		Tags:       tags,
		ForumID:    *p.ForumID,
	}
}
