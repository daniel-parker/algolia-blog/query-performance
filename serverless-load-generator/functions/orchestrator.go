package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/serverless-load-generator/internal/handlers"
)

func main() {
	lambda.Start(handlers.HandleOrchestrator)
}
