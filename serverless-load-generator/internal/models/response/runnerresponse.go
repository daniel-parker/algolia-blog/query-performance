package response

type RunnerResponse struct {
	AverageQueryTime float64
}
