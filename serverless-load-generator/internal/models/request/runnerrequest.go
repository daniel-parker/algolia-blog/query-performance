package request

type RunnerType int
type QueryComplexity int

const (
	RunnerTypeDatabase = iota
	RunnerTypeAlgolia
)

const (
	QuerySimple = iota
	QueryComplex
)

type RunnerRequest struct {
	BatchSize 	uint
	Type      	RunnerType
	Complexity 	QueryComplexity
}
