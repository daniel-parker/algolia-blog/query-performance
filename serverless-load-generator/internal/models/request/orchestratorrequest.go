package request

type OrchestratorRequest struct {
	RunnerType      RunnerType
	BatchSize		uint
	Batches			int
	Complexity 		QueryComplexity
}