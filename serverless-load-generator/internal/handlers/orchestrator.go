package handlers

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
	"github.com/aws/aws-sdk-go-v2/service/lambda/types"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/serverless-load-generator/internal/models/request"
	"log"
	"strconv"
	"sync"
	"time"

	"github.com/aws/aws-lambda-go/lambdacontext"
)

const (
	SleepyTimeMilliSeconds = 3900
)

// Lambda Handler
func HandleOrchestrator(ctx context.Context, req request.OrchestratorRequest) (float64, error) {

	cfg, err := config.LoadDefaultConfig(ctx)

	if err != nil {
		log.Fatalln(err)
	}

	lambdaClient := lambda.NewFromConfig(cfg)

	_, isLambdaContext := lambdacontext.FromContext(ctx)

	if err != nil {
		log.Fatalln(err)
	}

	// We collect the averages here
	averagesChan := make(chan float64, req.Batches)
	wg := &sync.WaitGroup{}

	log.Println("Batches: ", req.Batches)
	log.Println("Batch Size: ", req.BatchSize)

	switch req.RunnerType {
	case request.RunnerTypeDatabase:
		log.Println("Runner Mode: Database")
	case request.RunnerTypeAlgolia:
		log.Println("Runner Mode: Algolia")
	}

	// For each batch
	for i := 0; i < req.Batches; i++ {
		runnerRequest := &request.RunnerRequest{
			BatchSize: req.BatchSize,
			Type:      req.RunnerType,
			Complexity: req.Complexity,
		}

		payload, _ := json.Marshal(runnerRequest)

		// Are we running on AWS Lambda, or locally?
		if isLambdaContext {
			wg.Add(1)

			go func() {
				// Invoke a lambda runner function for this batch
				params := &lambda.InvokeInput{
					FunctionName:   &RunnerFuncName,
					InvocationType: types.InvocationTypeRequestResponse,
					LogType:        types.LogTypeNone,
					Payload:        payload,
				}
				o, err := lambdaClient.Invoke(ctx, params)

				if err != nil {
					log.Println(err)
				} else {
					average, err := strconv.ParseFloat(string(o.Payload), 64)

					log.Println("Average for batch: ", average)
					if err != nil {
						log.Println(err)
					} else {
						// Send the average back to the chan for later processing
						averagesChan <- average
					}
				}

				wg.Done()
			}()
		} else {
			callCtx, _ := context.WithDeadline(context.Background(), time.Now().Add(900 * time.Second))

			wg.Add(1)

			go func() {
				HandleRunner(callCtx, *runnerRequest)
				wg.Done()
			}()
		}
	}

	log.Println("Started ")

	wg.Wait()

	log.Println("Closing channel")
	close(averagesChan)

	log.Println("All runners have completed")
	log.Println("Calculating average")

	var average float64 = 0
	// Calculate the average of averages from each batch
	for anAverage := range averagesChan {
		if average != 0 {
			average = (average + anAverage) / 2
		} else {
			average = anAverage
		}
	}

	log.Println("Total Average: ", average)

	return average, nil
}
