package handlers

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/algolia/algoliasearch-client-go/v3/algolia/opt"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/algolia"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator/models"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/serverless-load-generator/internal/models/request"

	"github.com/mitchellh/mapstructure"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	simpleQuery = `
		select * from posts
		where body_tokens @@ to_tsquery(@query)
		order by created_at desc
		limit 100`
	complexQuery = `
		with recursive subforums as (
			select
				id,
				name,
				parent_id
			from forums
			where ID = @id
			union
				select
					f.id,
					f.name,
					f.parent_id
				from forums f
				inner join subforums sf on sf.ID = f.parent_id
		) select p.* from subforums sf
		join posts p on p.forum_id = sf.id
		where p.body_tokens @@ to_tsquery(@query)
		order by p.created_at desc
		limit 100
	`
)

// Lambda Handler
func HandleRunner(ctx context.Context, rr request.RunnerRequest) (float64, error) {
	dsn := os.Getenv("DB_CONN_STRING")

	var db *gorm.DB
	var forums []models.Forum

	// Open the database connection
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	myDB, _ := db.DB()
	// Close DB after this function ends
	defer myDB.Close()
	myDB.SetMaxIdleConns(10)
	myDB.SetMaxOpenConns(50)

	if err != nil {
		log.Panicln(err)
	}

	// Get all of the forums first
	result := db.WithContext(ctx).Find(&forums)
	if result.Error != nil {
		return 0, result.Error
	}

	lock := sync.Mutex{}
	var queryTimesInMs []int64

	wg := &sync.WaitGroup{}

	for i := uint(0); i < rr.BatchSize; i++ {
		wg.Add(1)
		go func() {
			var duration time.Duration
			var err error

			if rr.Type == request.RunnerTypeDatabase {
				duration, err = DoDBRequest(ctx, db, &forums, rr.Complexity)
			} else if rr.Type == request.RunnerTypeAlgolia {
				duration, err = DoAlgoliaRequest(ctx, db, &forums, rr.Complexity)
			}

			if err != nil {
				log.Println(err.Error())
			}

			lock.Lock()
			queryTimesInMs = append(queryTimesInMs, duration.Milliseconds())
			lock.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()

	lock.Lock()

	var sum int64

	for i := 0; i < len(queryTimesInMs); i++ {
		sum += queryTimesInMs[i]
	}

	average := float64(sum)/float64(len(queryTimesInMs))

	log.Println("Average: ", average)

	return average, nil
}

// Query the postgres database
func DoDBRequest(ctx context.Context, db *gorm.DB, forums *[]models.Forum, queryComplexity request.QueryComplexity) (time.Duration, error) {
	forumToUse := rand.Intn(len(*forums))

	var posts []models.Post

	var (
		before time.Time
		duration time.Duration
	)

	switch queryComplexity {
		case request.QuerySimple:
			before = time.Now()
			db.WithContext(ctx).Raw(simpleQuery, sql.Named("query", "git")).Find(&posts)
			duration = time.Since(before)
		case request.QueryComplex:
			before = time.Now()
			db.WithContext(ctx).Raw(complexQuery, sql.Named("id", (*forums)[forumToUse].ID), sql.Named("query", "git")).Find(&posts)
			duration = time.Since(before)
	}

	return duration, nil
}

// Query the algolia search index
func DoAlgoliaRequest(ctx context.Context, db *gorm.DB, forums *[]models.Forum, complexity request.QueryComplexity) (time.Duration, error) {
	forumToUse := rand.Intn(len(*forums))

	var posts []models.DenormalizedPost

	postIndex := algolia.NewPostIndex()

	var (
		params []interface{}
	)

	switch complexity {
		case request.QuerySimple:
			params = []interface{}{
				opt.HitsPerPage(100),
			}
		case request.QueryComplex:
			params = []interface{}{
				opt.TagFilter(fmt.Sprintf("%v", (*forums)[forumToUse].ID)),
				opt.HitsPerPage(100),
			}
	}

	before := time.Now()

	results, err := postIndex.Search("git", params)

	duration := time.Since(before)

	if err != nil {
		return duration, err
	}

	err = mapstructure.Decode(results.Hits, &posts)

	if err != nil {
		return duration, err
	}

	return duration, nil
}
