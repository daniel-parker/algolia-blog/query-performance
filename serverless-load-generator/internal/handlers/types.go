package handlers

import "github.com/aws/aws-lambda-go/events"

type APIResponse events.APIGatewayProxyResponse
type APIRequest events.APIGatewayProxyRequest

var (
	RunnerFuncName = "sls-load-gen-dev-runner"
)
