module gitlab.com/daniel-parker/algolia-blog/query-performance/serverless-load-generator

go 1.16

replace gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator => ../data-generator

require gitlab.com/daniel-parker/algolia-blog/query-performance/data-generator v0.0.0

require (
	github.com/algolia/algoliasearch-client-go/v3 v3.18.1
	github.com/aws/aws-lambda-go v1.23.0
	github.com/aws/aws-sdk-go-v2/config v1.1.6
	github.com/aws/aws-sdk-go-v2/service/lambda v1.2.2
	github.com/mitchellh/mapstructure v1.4.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.4
)
