package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/daniel-parker/algolia-blog/query-performance/serverless-load-generator/internal/handlers"
	"gitlab.com/daniel-parker/algolia-blog/query-performance/serverless-load-generator/internal/models/request"
)

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 120*time.Second)
	defer cancel()

	body := request.OrchestratorRequest{Batches: 1, BatchSize: 10, RunnerType: request.RunnerTypeDatabase, Complexity: request.QuerySimple}

	average, err := handlers.HandleOrchestrator(ctx, body)

	if err != nil {
		log.Println("Function was cancelled")
		return
	}

	log.Println("Average: ", average)
}
