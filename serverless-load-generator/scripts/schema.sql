CREATE TABLE post (
	id uuid DEFAULT uuid_generate_v1() PRIMARY KEY,
	title text NOT NULL,
	body text NOT NULL,
	authorId uuid NOT NULL
)

CREATE TABLE tag (
	id uuid DEFAULT uuid_generate_v1() PRIMARY KEY,
	name VARCHAR(100) NOT NULL
)

CREATE TABLE posttag (
	id uuid DEFAULT uuid_generate_v1() PRIMARY KEY,
	postId uuid NOT NULL,
	tagId uuid NOT NULL,
	CONSTRAINT fk_post
		FOREIGN KEY(postId)
			REFERENCES post(id),
	
	CONSTRAINT fk_tag
		FOREIGN KEY(tagId)
			REFERENCES tag(id)
)

CREATE TABLE "user" (
	id uuid DEFAULT uuid_generate_v1() PRIMARY KEY,
	userName TEXT NOT NULL,
	firstName TEXT NOT NULL,
	lastName TEXT NOT NULL
);

ALTER TABLE post
	ADD CONSTRAINT fk_user
		FOREIGN KEY(authorId)
			REFERENCES "user"("id");


/* INSERT INTO post ((SELECT uuid_generate_v1()), "Bleh bleh", "My body") */