with recursive subforums as (
    select
        id,
        name,
        parent_id
    from forums
    where ID = 2
    union
        select
            f.id,
            f.name,
            f.parent_id
        from forums f
        inner join subforums sf on sf.ID = f.parent_id
) select p.* from subforums sf
join posts p on p.forum_id = sf.id
-- where to_tsvector(p.body) @@ to_tsquery('git')
where p.body_tokens @@ to_tsquery('git')
order by p.created_at desc
limit 50

