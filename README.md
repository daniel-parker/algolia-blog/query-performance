# SlackerNews

This is the code for the article: {+ article link here once posted +}

## What does this do?
Generates fake forum data and benchmarks different queries against that forum database

## Requirements
* Golang 1.16.3
* serverless framework
* export `DB_CONN_STRING`, `ALGOLIA_APP_ID` and `ALGOLIA_API_KEY`

## Running
### data-generator
1. Update the [number of posts to generate](data-generator/internal/generators/posts.go/#L67)
1. `go run cmd/run.go`

### serverless-load-generator
#### Local
1. `go run cmd/run.go`

#### AWS
1. Setup the AWS SDK & Serverless Framework
1. `make deploy`
